# QuickTerminal

#### Description
GUI tool for Terminal in MacOS. The basic principle of this software is to store your command, and you can run it by one click.
Python scripts are also supported, you can add some light script with its parameters which can be mutually exclusive. Find it in 'Scripts' tab.
In CommandLine tab you can choose your command to run.
In Add tab you can choose to add scripts or commands, type or paste in the editline, or even drag files on to it.

Interact with the terminal are not supported.


#### Software Architecture
MVC..?