import json
from PathHandler import CONFIGS_PATH


class JsonHandler:
    @staticmethod
    def read_json(path):
        with open(path, 'r', encoding='utf8') as fp:
            json_data = json.load(fp)
            return json_data

    @staticmethod
    def write_json(dictionary, path):
        with open(path, 'w', encoding='utf8') as fp:
            json.dump(dictionary, fp, ensure_ascii=False)

    @staticmethod
    def load_config_data():
        scripts = JsonHandler.read_json(CONFIGS_PATH)["scripts"]
        pythons = JsonHandler.read_json(CONFIGS_PATH)["pythons"]
        favourites = JsonHandler.read_json(CONFIGS_PATH)["favourites"]
        return scripts, pythons, favourites

    @staticmethod
    def write_config_data(json_file):
        JsonHandler.write_json(json_file, CONFIGS_PATH)
