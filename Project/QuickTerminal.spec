# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['/Users/mrop404/Documents/Project_Python/QuickTerminal/Project/mac_app.py'],
             pathex=['/Users/mrop404/Documents/Project_Python/QuickTerminal/Project'],
             binaries=[],
             datas=[('Resource/Json', './Resource/Json'),('Resource/Scripts', './Resource/Scripts')],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts, 
          [],
          exclude_binaries=True,
          name='QuickTerminal',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None , icon='/Users/mrop404/Documents/Project_Python/QuickTerminal/Project/Resource/icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas, 
               strip=False,
               upx=True,
               upx_exclude=[],
               name='QuickTerminal')
app = BUNDLE(coll,
             name='QuickTerminal.app',
             icon='/Users/mrop404/Documents/Project_Python/QuickTerminal/Project/Resource/icon.ico',
             bundle_identifier=None)
