### Model.py
Model类：

PassObject类：
抽象基类，是在反序列化的时候依据不同类型所创建的不同的类。
每一个对象在创建与初始化的时候就确定要show和要run的cmd命令。



### 互斥Args按钮点击

一个arg按钮UI被点击，调用controller的事件，传入按钮名称，进行互斥关闭和新值添加，
同时通知cmd命令添加。model被改变，将活跃 哒哒arg的列表传到ui，ui执行uncheck操作。


### 清除或更改当前脚本

清除按钮UI被点击，调用controller的cmdClear事件，以当前脚本名称调用controller的改变当前脚本方法。
选择了不同的py脚本，以新选择的脚本名称调用controller的改变当前脚本方法。
