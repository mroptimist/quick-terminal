import os
import sys


def my_prepare(arg_count, usage, file_input):
    """
    Args:
        arg_count: 程序共有多少参数
        file_input: 要进行操作的文件或文件夹
        usage: 要打印的使用方法
    Returns:
        输入的是否是文件夹(isDir);输入的原始文件夹(path);文件夹或文件所在位置(save);输出文件的保存位置(file);输入文件或路径名称(name)
    """
    if len(sys.argv) < arg_count:
        print(usage)
        exit(-1)

    if os.path.isdir(file_input):
        isdir = True
        read_path = file_input
        save_path = file_input
        save_file = save_path + os.sep + 'save_file.txt'
        path_name = save_path[save_path.rfind(os.sep) + 1:]
    else:
        isdir = False
        read_path = file_input
        save_path = read_path[0:read_path.rfind(os.sep)]
        save_file = save_path + +os.sep + 'save_file.txt'
        path_name = read_path[read_path.rfind(os.sep) + 1:read_path.rfind(".")]
    return {"isdir": isdir, "path": read_path, "save": save_path, "file": save_file, "name": path_name}


def my_script_end(pre, last_time, outfile=False):
    """
    Args:
        *pre: my_prepare() 返回的元组
        last_time: 程序持续的时间
        outfile: 是否打印输出文件的信息
    """
    if outfile:
        print("程序在{}秒后结束，大小:{}MB，生成文件:{}".format(
            round(last_time, 3),
            round(os.path.getsize(pre["file"]) / (1024 * 1024), 3), pre["save"]
        ))
    else:
        print("程序在{}秒后结束".format(
            round(last_time, 3)
        ))
    pass
