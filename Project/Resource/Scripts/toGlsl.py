import sys
import os

if len(sys.argv) < 2:
    print("Usage: toGlsl dirpath [-spv]")
    exit(-1)

curDir = os.getcwd()
asDir = "/Users/mrop404/Documents/[Git]Repo/SPIRV-Tools/build/install/bin/spirv-as"
crossDir = "/Users/mrop404/Documents/[Git]Repo/SPIRV-Cross/build/spirv-cross"


def process(path):
    # 读入文件目录
    # 如果argv是个路径返回路径，如果是文件名，组合成路径
    readFile = path
    fileName = readFile[readFile.rfind("/"):readFile.rfind(".")]
    fileFormat = ".vert" if "Vertex" in readFile else ".frag"

    # 生成目录
    dstDir = readFile[:readFile.rfind(".")]
    # 生成文件名
    spvName = fileName+".spv"
    # 生成Shader名
    shaderName = fileName+fileFormat
    # 创建生成目录
    if not os.path.isdir(dstDir):
        os.mkdir(dstDir)
    else:
        os.mkdir(dstDir)

    asCmd = "%s %s -o %s" % (asDir, readFile, dstDir+spvName)
    print(asCmd)
    os.system(asCmd)
    crossCmd = "%s %s --output %s" % (crossDir,
                                      dstDir+spvName,
                                      dstDir+shaderName)
    print(crossCmd)
    os.system(crossCmd)

    if len(sys.argv) > 2 and sys.argv[2] == "-spv":
        pass
    else:
        os.remove(dstDir+spvName)


for root, dirs, files in os.walk(sys.argv[1]):
    for file in files:
        if file.endswith(".glsl"):
            process(root + "/" + file)
