import os

# 被清理的格式
Format = {
    ".XML"
}


def del_files(readpath):
    for root, dirs, files in os.walk(readpath):
        for name in files:
            for f in Format:
                if name.endswith(f):
                    os.remove(os.path.join(root, name))
                    print("delete "+name)
    for root, dirs, files in os.walk(readpath, topdown=False):
        if not files and not dirs:
            print("removeDir " + root)
            os.rmdir(root)


if __name__ == "__main__":
    # 绝对路径
    # readpath = r'/Users/bytedance/Desktop/毕设/sky'
    readpath = r'/Users/mrop404/Desktop/CLIP'
    del_files(readpath)
