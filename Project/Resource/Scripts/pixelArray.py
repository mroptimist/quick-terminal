import time
import os
import sys
import cv2

# 模式，通道个数，间隔数
dict = {'-R': {'count': 1, 'channel': '0', 'dist': 6},
        '-G': {'count': 1, 'channel': '1', 'dist': 6},
        '-B': {'count': 1, 'channel': '2', 'dist': 6},
        "-RG": {'count': 1, 'channel': '01', 'dist': 10},
        "-RB": {'count': 1, 'channel': '02', 'dist': 10},
        "-GB": {'count': 1, 'channel': '12', 'dist': 10},
        "-RGB": {'count': 1, 'channel': '012', 'dist': 14},
        "-RGBA": {'count': 1, 'channel': '0123', 'dist': 18}}


def readImg(readpath, savepath, mode, reverse):
    '''
    基本描述
        输入目标图像路径，和它的通道数，在图片的同级目录下生成一个txt图片RGB
    详细描述
        文件会附加写入
    Args:
        readpath (str): 图片路径
        savepath (int): 保存路径，暂时为图片同级目录
        mode (dict): 图片通道模式字典
    Returns:
    '''
    if mode['channel'] == '0123':                           # 如果有Alpha就读入Alpha
        im = cv2.imread(readpath, cv2.IMREAD_UNCHANGED)
    else:                                                   # 不然正常读入RGB
        im = cv2.imread(readpath)
    im = cv2.imread(readpath)
    f = open(savepath, 'a+', buffering=1)                   # 文件读写
    tempChannel = im[:, :, 0].copy()                        # RB通道互换
    im[:, :, 0] = im[:, :, 2]
    im[:, :, 2] = tempChannel
    h, w, c = im.shape                                      # 获取图片的宽和高

    channelList = []
    for a in mode['channel']:                               # 取出图片的通道，放到列表里
        channelList.append(im[:, :, int(a)])

    pCmd = ('%s' % '%3d,' * len(channelList))[:-1]          # 根据通道数，可变数量的命令
    pArg = ''
    for i in range(len(channelList)):                       # 可变数量的参数
        pArg += "channelList[%d][x, y] - 255 * reverse" % i
        pArg += ','
    for x in range(h):                                      # 遍历高度的像素
        f.write("==> %4d  " % (x+1))                        # 打印行标
        for y in range(w):                                  # 遍历宽度的像素
            element = ('[%s] ' % pCmd) % eval(pArg[:-1])    # 使用参数和命令生成一个像素 "[ 97,206] "
            f.write(element)                                # 写入像素""
        f.write("\n")                                       # 换行

    for j in ["∆", "|"]:                                    # 打印末行行标"[channelList[0][x, y] - 255 * reverse,channelList[1][x, y] - 255 * reverse]"
        f.write(' ' * 10)
        for k in range(w):
            f.write(str(j).center(mode['dist']))
        f.write("\n")
    # 打印末行序号
    f.write(sys.argv[2].ljust(6) + ' ' * 4)
    for k in range(w):
        f.write(str(k+1).center(mode['dist']))
    f.write("\n\n")
    f.close()


if __name__ == '__main__':
    # 计时开始
    start = time.time()

    PrepareWork = True
    while PrepareWork:
        if len(sys.argv) < 3:
            print("Usage: This .py file/dir mode [r]")
            exit(-1)
        else:
            inputArg = sys.argv[2]
            if not inputArg in dict.keys():
                print("Usage: Give right mode in: [R, G, B, RG, RGB, RGBA]")
                exit(-1)

        input = sys.argv[1]
        reverse = 0
        if len(sys.argv) == 4 and sys.argv[3] == '-r':
            reverse = 1

        isdir = False
        if os.path.isdir(input):
            isdir = True
            readpath = input
            savepath = input
            savefile = savepath + os.sep+'savefile.txt'
            pathname = savepath[savepath.rfind(os.sep) + 1:]
        else:
            isdir = False
            readfile = input
            savepath = readfile[0:readfile.rfind(os.sep)]
            filename = readfile[readfile.rfind(os.sep)+1:readfile.rfind(".")]
            savefile = savepath + os.sep+filename+'.txt'
        break

    start = time.time()

    # ---codeblock
    readImg(readfile, savefile, dict[inputArg], reverse)

    end = time.time()
    print("程序在{}秒后结束，大小:{}MB，生成文件:{}".format(
        round(end-start, 3),
        round(os.path.getsize(savepath if isdir else savefile)/(1024 * 1024), 3),
        savepath if isdir else savefile
    ))
