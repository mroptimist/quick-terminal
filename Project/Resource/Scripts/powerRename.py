import os
import sys
import time

import Project.Resource.MyPyLibrary.MyModule as MrOptimist


def power_rename(path, data, mode=0):
    if mode == 0:  # 匹配删除模式
        for root, dirs, files in os.walk(path):
            for name in files:
                if not name.find(data) == -1:
                    os.rename(root + os.sep + name, root + os.sep + name.replace(data, ''))
    if mode == 1:  # 前缀删除模式
        for root, dirs, files in os.walk(path):
            for name in files:
                os.rename(root + os.sep + name, root + os.sep + name[int(data):])


if __name__ == '__main__':
    prepare = MrOptimist.my_prepare(3, "[dir] [arg] [mode]", sys.argv[1])

    start = time.time()
    # ---codeblock
    power_rename(sys.argv[1], sys.argv[2], mode=1 if sys.argv[3] == "-m" else 0)
    # ---codeblock
    end = time.time()

    MrOptimist.my_script_end(prepare, end - start)
