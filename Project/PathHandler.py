import sys
import os.path as path


def add_escape(value):
    reserved_chars = r'''?&|!{}[]()^~*:\\"'+- '''
    replace = ['\\' + l for l in reserved_chars]
    trans = str.maketrans(dict(zip(reserved_chars, replace)))
    return value.translate(trans)


def resource_path(relative_path, convert=False):
    if getattr(sys, 'frozen', False):       # 打包后的地址
        base_path = sys._MEIPASS
        if convert:
            base_path = add_escape(base_path)
    else:
        base_path = path.abspath(".")     # 未打包的地址
        base_path = path.join(base_path, "Project")
        if convert:
            base_path = add_escape(base_path)
    return path.join(base_path, relative_path)


CONFIGS_PATH = resource_path("Resource/Json/configs.json")
SCRIPTS_PATH = resource_path("Resource/Scripts/", True)
