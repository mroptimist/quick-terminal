import os
import sys
import warnings

from PyQt5 import QtCore
from PyQt5.QtCore import QObject, pyqtSlot
from JsonHandler import JsonHandler
from threading import Thread
import subprocess
import platform

from Model import PassArg, PassFave, PassInput, PassPython, PassScript
from PathHandler import CONFIGS_PATH, SCRIPTS_PATH, add_escape


class Controller(QObject):
    def __init__(self, model) -> None:
        super().__init__()
        self._model = model
        # print(CONFIGS_PATH)
        # print(SCRIPTS_PATH)

    def eventFilter(self, obj, event):
        """
        处理窗体内出现的事件，如果有需要则自行添加if判断语句；
        目前已经实现将拖到控件上文件的路径设置为控件的显示文本；
        """
        if event.type() == QtCore.QEvent.DragEnter:
            event.accept()
        if event.type() == QtCore.QEvent.Drop:
            md = event.mimeData()
            if md.hasUrls():
                url = md.urls()[0]
                path = url.toLocalFile()
                path = add_escape(path)
                if obj.objectName() == "cmdLineEdit":
                    self.change_cmd(PassInput(path))
                elif obj.objectName() == "lineEdit":
                    self.change_current_cmd_to_add(path)
                return True
        return super().eventFilter(obj, event)

    def data_init(self):
        """程序启动之后执行的json加载数据初始化"""
        self.pythons_reload()
        self.scripts_reload()
        self.fave_cmd_reload()
        self.change_cmd_clear()

    # <editor-fold desc="CMD处理">
    def change_cmd(self, value):
        """ 传入PassObject 处理待添加或去除的参数 """
        arg_to_run = value.arg_to_run()
        arg_to_show = value.arg_to_show()

        if True if len(arg_to_run) == 0 else False:  # 初始空值返回
            return
        _cmd_to_show = self._model.cmd_to_show
        _cmd_to_run = self._model.cmd_to_run

        if isinstance(value, PassArg):
            if arg_to_show not in _cmd_to_show:
                _cmd_to_show += " " + arg_to_show
                _cmd_to_run += " " + arg_to_run
            else:
                _cmd_to_show = _cmd_to_show.replace(" " + arg_to_show, "")
                _cmd_to_run = _cmd_to_run.replace(" " + arg_to_run, "")
        elif isinstance(value, PassScript):
            # 遍历当前python脚本是否在cmd中，如果在就替换，如果不再就直接加进去
            is_found = False
            for script in self._model.scripts:
                if script.arg_to_show() in _cmd_to_show:
                    _cmd_to_show = _cmd_to_show.replace(
                        script.arg_to_show(), arg_to_show)
                    _cmd_to_run = _cmd_to_run.replace(
                        script.arg_to_run(), arg_to_run)
                    is_found = True
            if not is_found:
                _cmd_to_show += " " + arg_to_show + ".py"
                _cmd_to_run += " " + arg_to_run
        elif isinstance(value, PassPython):
            # 遍历当前python解释器是否在cmd中，如果在就替换，如果不再就直接加进去
            is_found = False
            for python in self._model.pythons:
                if python.arg_to_show() in _cmd_to_show:
                    _cmd_to_show = _cmd_to_show.replace(
                        python.arg_to_show(), arg_to_show)
                    _cmd_to_run = _cmd_to_run.replace(
                        python.arg_to_run(), arg_to_run)
                    is_found = True
            if not is_found:
                _cmd_to_show += " " + arg_to_show
                _cmd_to_run += " " + arg_to_run
        elif isinstance(value, PassInput):
            # 如果是拖入的值，则直接附加即可
            _cmd_to_show += " " + arg_to_show
            _cmd_to_run += " " + arg_to_run

        # 将值传入单例模型来生效
        self._model.cmd_to_show = _cmd_to_show
        self._model.cmd_to_run = _cmd_to_run

    @pyqtSlot(bool)
    def change_cmd_clear(self, value=""):
        """value有值则是tab切换重置，没有值则是切换脚本或解释器导致的重置"""
        self._model.cmd_to_show = ""
        self._model.cmd_to_run = ""
        self._model.active_args = []
        if value == "script":
            self.change_current_script(self._model.active_script.arg_to_show())
        elif value == "python":
            self.change_current_python(self._model.active_python.arg_to_show())
        else:
            if self._model.active_tab == 0:
                self.change_current_python(
                    self._model.active_python.arg_to_show())
                self.change_current_script(
                    self._model.active_script.arg_to_show())
            elif self._model.active_tab == 1:
                self.change_current_fave_cmd(
                    self._model.active_fave.arg_to_show())
            elif self._model.active_tab == 2:
                self._model.active_usage = "拖入文件，键入命令，然后点击添加命令"
                self._model.cmd_to_add = ""

    @pyqtSlot(bool)
    def exec_cmd(self):
        """ 执行当前cmd_to_run中的命令 """
        win = platform.system() == "Windows"
        mac = platform.system() == "Darwin"
        print("执行指令：" + self._model.cmd_to_run)
        if self._model.active_tab == 0:
            def func(cmd):
                p = subprocess.run(
                    cmd, shell=True, executable="/bin/zsh", stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                print(p.stdout.decode("utf-8"))

            t = Thread(target=func, args=(self._model.cmd_to_run,))
            # start = time.time()
            t.start()
            t.join()
            # end = time.time()
            # print(end-start)
        else:
            def func(cmd):
                p = subprocess.Popen(
                    cmd, shell=True, executable="/bin/zsh", stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                outs, errs = p.communicate(timeout=5)
                print(outs.decode("gbk" if win else "utf8", "ignore"))

            t = Thread(target=func, args=(self._model.cmd_to_run,))
            t.start()
            t.join()

    # </editor-fold>

    # <editor-fold desc="Model数据处理">
    @pyqtSlot(str)
    def change_current_python(self, value):
        """更改当前使用的python解释器"""
        for python in self._model.pythons:
            if python.arg_to_show() == value:
                self.change_cmd(python)
                self._model.active_python = python
                break

    @pyqtSlot(str)
    def change_current_script(self, value):
        """ 变更当前选择的脚本，并清空参数 """
        for script in self._model.scripts:
            if script.arg_to_show() == value:
                self.change_cmd(script)  # 添加传入的Python脚本
                self._model.active_script = script  # 找寻当前脚本
                self._model.args = script.args()
                for arg in self._model.active_args:
                    self.change_cmd(PassArg(arg))
                self._model.active_args = []
                self._model.active_usage = script.usage()
                break

    @pyqtSlot(str)
    def change_active_args(self, value):
        """ 输入一个arg
            如果有互斥的就取消点亮互斥并点亮此arg
            如果此arg已经被点亮，就取消点亮此arg """
        active_args = self._model.active_args
        is_exist = False
        if value in active_args:
            is_exist = True
        if is_exist:  # 如果点击一个已经存在的，去掉即可
            self.change_cmd(PassArg(value))
            active_args.remove(value)
        else:  # 如果点击一个不存在的，先寻找冲突，再点击
            conflict_args = []
            for args in self._model.args:  # 遍历二级列表
                for arg in args:  # 遍历一级列表
                    if value == arg:
                        conflict_args = args  # 找到冲突的一级列表
            for arg in conflict_args:
                if arg != value and arg in self._model.active_args:
                    active_args.remove(arg)
                    self.change_cmd(PassArg(arg))  # 去除冲突
            self.change_cmd(PassArg(value))
            active_args.append(value)
        self._model.active_args = active_args
        for arg in self._model.active_args:
            warnings.warn(arg)

    @pyqtSlot(str)
    def change_current_fave_cmd(self, value):
        self._model.cmd_to_run = value
        self._model.cmd_to_show = value
        for fave in self._model.faves:
            if value == fave.arg_to_show():
                self._model.active_fave = fave
                self._model.active_usage = fave.usage()
                return
        pass

    @pyqtSlot(int)
    def change_current_tab(self, value):
        self._model.active_tab = value
        self.change_cmd_clear()

    @pyqtSlot(str)
    def change_current_cmd_to_add(self, value):
        self._model.cmd_to_add = value

    # </editor-fold>

    # <editor-fold desc="Json反序列化">
    def pythons_reload(self):  # 加载解释器路径
        pythons = JsonHandler.load_config_data()[1]
        pythons_list = []
        for python in pythons:
            pythons_list.append(PassPython(python["name"], python["path"]))
        self._model.active_python = pythons_list[0]
        self._model.pythons = pythons_list

    def scripts_reload(self):  # 加载脚本
        scripts = JsonHandler.load_config_data()[0]
        scripts_list = []
        for script in scripts:
            pyfile = SCRIPTS_PATH + script["pyfile"]
            scripts_list.append(PassScript(
                script["id"], pyfile, script["usage"], script["args"]))
        self._model.active_script = scripts_list[0]
        self._model.scripts = scripts_list

    def fave_cmd_reload(self):  # 加载收藏命令
        faves = JsonHandler.load_config_data()[2]
        faves_list = []
        for fave in faves:
            faves_list.append(
                PassFave(fave["command"], fave["description"], fave["usage"]))
        self._model.active_fave = faves_list[0]
        self._model.faves = faves_list

    # </editor-fold>

    # <editor-fold desc="Json序列化(添加删除项)">
    def scripts_serialize(self, add=True):
        scripts, pythons, favourites = JsonHandler.load_config_data()
        if add:
            _cmd_to_add = self._model.cmd_to_add
            pyfile = _cmd_to_add[_cmd_to_add.rfind(os.sep) + 1:]
            name = pyfile[:pyfile.rfind('.')]
            usage = ""
            args = []
            scripts.append({'id': name, 'pyfile': pyfile,
                           'usage': usage, 'args': args})
        else:
            name = self._model.active_script.arg_to_show()
            for i, script in enumerate(scripts):
                if script["id"] == name:
                    del scripts[i]
                    break
        JsonHandler.write_config_data(
            {'pythons': pythons, 'scripts': scripts, 'favourites': favourites})
        self.scripts_reload()
        self.change_cmd_clear()

    def fave_serialize(self, add=True):
        scripts, pythons, favourites = JsonHandler.load_config_data()
        if add:  # 增加条目
            command = self._model.cmd_to_add
            description = ""
            usage = ""
            favourites.append(
                {'command': command, 'description': description, 'usage': usage})
        else:  # 删除条目
            command = self._model.cmd_to_run
            for i, fav in enumerate(favourites):
                if fav["command"] == command:
                    del favourites[i]
                    break
        JsonHandler.write_config_data(
            {'pythons': pythons, 'scripts': scripts, 'favourites': favourites})
        self.fave_cmd_reload()
        self.change_cmd_clear()

    def fave_add(self):
        self.fave_serialize(True)

    def script_add(self):
        if self._model.cmd_to_add[self._model.cmd_to_add.rfind(os.sep) + 1:].find(".py") == -1:
            print("非python脚本")
            self.change_cmd_clear()
            return
        print("cp -rf " + self._model.cmd_to_add + " " + SCRIPTS_PATH)
        self.exec_quick_cmd(
            "cp -rf " + self._model.cmd_to_add + " " + SCRIPTS_PATH)
        self.scripts_serialize(True)

    def fave_remove(self):
        self.fave_serialize(False)

    def script_remove(self):
        self.exec_quick_cmd("rm " + self._model.active_script.arg_to_run())
        self.scripts_serialize(False)

    # </editor-fold>

    # <editor-fold desc="需要快速执行的一些指令">
    def exec_quick_cmd(self, cmd):
        self._model.cmd_to_run = cmd
        self.exec_cmd()

    def config_open(self):
        self.exec_quick_cmd("open " + CONFIGS_PATH)

    def cp_file(self):
        self.exec_quick_cmd(
            "cp -rf" + self._model.cmd_to_add + " " + SCRIPTS_PATH)
    # </editor-fold>
