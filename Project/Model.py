from abc import abstractmethod
import os
from PyQt5.QtCore import QObject, pyqtSignal


class Model(QObject):
    def __init__(self) -> None:
        super().__init__()
        self._cmd_to_show = ""  # 当前要显示的CMD
        self._cmd_to_run = ""  # 当前要运行的CMD
        self._cmd_to_add = ""  # 当前要添加的CMD
        self._scripts = []  # 当前程序中所有脚本
        self._active_script = None  # 当前选择脚本
        self._active_usage = ""  # 当前命令的描述
        self._args = []  # 当前脚本的二级参数列表
        self._active_args = []  # 当前程序被点亮的参数
        self._pythons = []  # 当前程序中存储python字典列表
        self._active_python = None  # 当前选择的解释器
        self._active_tab = 0  # 当前控制台输出
        self._faves = []  # 当前程序中存储的收藏命令
        self._active_fave = None  # 当前选中的收藏命令

    # <editor-fold desc="Pyqt信号槽，当单例model中的变量发生值变更时发出信号">
    cmd_to_show_changed = pyqtSignal(str)
    cmd_to_add_changed = pyqtSignal(str)
    scripts_changed = pyqtSignal(list)
    usage_changed = pyqtSignal(str)
    pythons_changed = pyqtSignal(list)
    args_changed = pyqtSignal(list)
    active_args_changed = pyqtSignal(list)
    faves_changed = pyqtSignal(list)
    active_tab_changed = pyqtSignal(int)
    # </editor-fold>

    # <editor-fold desc="model中的私有变量的getter与setter，某些变量的setter会发射信号">
    @property
    def cmd_to_show(self):
        return self._cmd_to_show

    @cmd_to_show.setter
    def cmd_to_show(self, value):
        self._cmd_to_show = value
        self.cmd_to_show_changed.emit(value)

    @property
    def cmd_to_run(self):
        return self._cmd_to_run

    @cmd_to_run.setter
    def cmd_to_run(self, value):
        self._cmd_to_run = value

    @property
    def cmd_to_add(self):
        return self._cmd_to_add

    @cmd_to_add.setter
    def cmd_to_add(self, value):
        self._cmd_to_add = value
        self.cmd_to_add_changed.emit(value)

    @property
    def scripts(self):
        return self._scripts

    @scripts.setter
    def scripts(self, value):
        self._scripts = value
        self.scripts_changed.emit(value)

    @property
    def active_script(self):
        return self._active_script

    @active_script.setter
    def active_script(self, value):
        self._active_script = value

    @property
    def active_usage(self):
        return self._active_usage

    @active_usage.setter
    def active_usage(self, value):
        self._active_usage = value
        self.usage_changed.emit(value)

    @property
    def pythons(self):
        return self._pythons

    @pythons.setter
    def pythons(self, value):
        self._pythons = value
        self.pythons_changed.emit(value)

    @property
    def active_python(self):
        return self._active_python

    @active_python.setter
    def active_python(self, value):
        self._active_python = value

    @property
    def args(self):
        return self._args

    @args.setter
    def args(self, value):
        self._args = value
        self.args_changed.emit(value)

    @property
    def active_args(self):
        return self._active_args

    @active_args.setter
    def active_args(self, value):
        self._active_args = value
        self.active_args_changed.emit(value)

    @property
    def active_tab(self):
        return self._active_tab

    @active_tab.setter
    def active_tab(self, value):
        self._active_tab = value
        self.active_tab_changed.emit(value)
        pass

    @property
    def active_fave(self):
        return self._active_fave

    @active_fave.setter
    def active_fave(self, value):
        self._active_fave = value
        pass

    @property
    def faves(self):
        return self._faves

    @faves.setter
    def faves(self, value):
        self._faves = value
        self.faves_changed.emit(value)
        pass
    # </editor-fold>


class PassObject:
    def __init__(self) -> None:
        self._arg_to_show = ""
        self._arg_to_run = ""

    @classmethod
    @abstractmethod
    def data_init(cls):
        pass

    def arg_to_show(self):
        return self._arg_to_show

    def arg_to_run(self):
        return self._arg_to_run


class PassArg(PassObject):
    def __init__(self, name) -> None:
        super().__init__()
        self._name = name
        self.data_init()

    def data_init(self):
        self._arg_to_show = self._name
        self._arg_to_run = self._name


class PassInput(PassObject):
    def __init__(self, path) -> None:
        super().__init__()
        self._name = path
        self._path = path
        self.data_init()

    def data_init(self):
        if self._path.rfind(os.sep) == len(self._path) - 1:
            self._name = self._path[self._path.rfind(
                os.sep, 0, len(self._path) - 1) + 1:]
        elif self._path.rfind(os.sep) != -1:
            self._name = self._path[self._path.rfind(os.sep) + 1:]
        else:
            print("file name wrong")
        self._arg_to_show = self._name
        self._arg_to_run = self._path


class PassPython(PassObject):
    def __init__(self, name, path) -> None:
        super().__init__()
        self._name = name
        self._path = path
        self.data_init()

    def data_init(self):
        self._arg_to_show = self._name
        self._arg_to_run = self._path


class PassScript(PassObject):
    def __init__(self, name, pyfile, usage, args) -> None:
        super().__init__()
        self._id = name
        self._pyfile = pyfile
        self._args = args
        self._usage = usage
        self.data_init()

    def args(self):
        return self._args

    def usage(self):
        return self._usage

    def data_init(self):
        self._arg_to_show = self._id
        self._arg_to_run = self._pyfile


class PassFave(PassObject):
    def __init__(self, command, description, usage) -> None:
        super().__init__()
        self._command = command
        # self._description = description + ": " + usage
        self._usage = description + ": " + usage
        self.data_init()

    # def description(self):
    #     return self._description

    def usage(self):
        return self._usage

    def data_init(self):
        self._arg_to_show = self._command
        self._arg_to_run = self._command
