from PyQt5.QtCore import pyqtSlot, QSignalMapper
from PyQt5.QtWidgets import QMainWindow, QAction, QMenu
from PyQt5 import QtWidgets, QtGui, QtCore
from mainUI import Ui_MainWindow
from EmittingStr import EmittingStr
import sys


# 第二是View Change


class View(QMainWindow):
    def __init__(self, model, controller) -> None:
        super().__init__()
        self._model = model
        self._controller = controller
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)

        # 控制Print和报错的显示位置
        sys.stdout = EmittingStr(textWritten=self.output_written)
        sys.stderr = EmittingStr(textWritten=self.output_written)

        # 处理多按钮链接同一个信号
        self._mapper = QSignalMapper(self)
        self._mapper.mapped[str].connect(self._controller.change_active_args)

        # 注册事件回调，事件算为改变UI
        self._ui.cmdLineEdit.installEventFilter(self._controller)
        self._ui.lineEdit.installEventFilter(self._controller)

        # 监听UI改变信号（UI输入改变模型）
        self._ui.cmdClearButton.clicked.connect(self._controller.change_cmd_clear)
        self._ui.cmdAddClear.clicked.connect(self._controller.change_cmd_clear)

        self._ui.addCmdButton.clicked.connect(self._controller.fave_add)
        self._ui.addScriptButton.clicked.connect(self._controller.script_add)
        self._ui.lineEdit.textChanged.connect(self._controller.change_current_cmd_to_add)
        self._ui.favList.customContextMenuRequested[QtCore.QPoint].connect(self.fave_list_context)
        self._ui.scriptsComboBox.customContextMenuRequested[QtCore.QPoint].connect(self.script_list_context)
        self._ui.openConfigButton.clicked.connect(self._controller.config_open)

        self._ui.execButton.clicked.connect(self._controller.exec_cmd)
        self._ui.actionReloadScripts.triggered.connect(self._controller.scripts_reload)
        self._ui.actionReloadPythons.triggered.connect(self._controller.pythons_reload)
        self._ui.scriptsComboBox.currentIndexChanged[str].connect(self._controller.change_current_script)
        self._ui.pythonsComboBox.currentIndexChanged[str].connect(self._controller.change_current_python)
        self._ui.inputWidget.currentChanged[int].connect(self._controller.change_current_tab)
        self._ui.favList.currentTextChanged.connect(self._controller.change_current_fave_cmd)
        self._ui.outputClearButton.clicked.connect(self.on_print_text_clear)

        # 监听模型改变信号（模型内部改变来操纵UI）
        self._model.cmd_to_show_changed.connect(self.on_cmd_to_show_changed)  # 变更显示命令
        self._model.cmd_to_add_changed.connect(self.on_cmd_to_add_changed)  # 变更显示命令
        self._model.scripts_changed.connect(self.on_scripts_reload)  # 刷新脚本目录
        self._model.pythons_changed.connect(self.on_pythons_reload)  # 刷新解释器目录
        self._model.args_changed.connect(self.on_scripts_changed)  # 变更当前脚本
        self._model.usage_changed.connect(self.on_usage_changed)  # 变更当前脚本
        self._model.active_args_changed.connect(self.on_button_uncheck)  # 变更当前参数
        self._model.faves_changed.connect(self.on_faves_cmd_reload)  # 变更收藏列表

        # 初始化数据
        self._controller.data_init()

    # 以下是根据数据操纵UI
    @pyqtSlot(str)
    def on_cmd_to_show_changed(self, value):
        """更新命令行显示的文本UI变更"""
        self._ui.cmdLineEdit.setText(value)

    @pyqtSlot(str)
    def on_cmd_to_add_changed(self, value):
        """更新命令行显示的文本UI变更"""
        self._ui.lineEdit.setText(value)

    @pyqtSlot(list)
    def on_scripts_reload(self, value):
        """重新加载所有脚本时UI变更"""
        self._ui.scriptsComboBox.clear()
        self._controller.change_cmd_clear("script")
        for script in value:
            self._ui.scriptsComboBox.addItem(script.arg_to_show())

    @pyqtSlot(list)
    def on_pythons_reload(self, value):
        """重新加载所有解释器时UI变更"""
        self._ui.pythonsComboBox.clear()
        self._controller.change_cmd_clear("python")
        for python in value:
            self._ui.pythonsComboBox.addItem(python.arg_to_show())

    @pyqtSlot(list)
    def on_faves_cmd_reload(self, value):
        """重新加载所有收藏脚本时UI变更"""
        self._ui.favList.clear()
        self._controller.change_cmd_clear()
        for fave in value:
            item = QtWidgets.QListWidgetItem(fave.arg_to_show())
            self._ui.favList.addItem(item)
        pass

    @pyqtSlot(list)
    def on_scripts_changed(self, value):
        """切换当前脚本时UI变更"""
        k = 0
        for btn in self._ui.groupBox.findChildren(QtWidgets.QPushButton):  # 删除所有
            k = 0 if k > 3 else k + 1
            self._ui.gridLayout.removeWidget(btn)
            btn.deleteLater()
            del btn

        for i in range(3 - k):  # 解决按钮位置错乱的问题
            button = QtWidgets.QPushButton()
            self._ui.gridLayout.addWidget(button)
            self._ui.gridLayout.removeWidget(button)
            button.deleteLater()
            del button

        for conflict_args in value:
            for arg in conflict_args:
                button = QtWidgets.QPushButton(arg, self)

                button.clicked.connect(self._mapper.map)
                self._mapper.setMapping(button, button.text())

                button.setCheckable(True)
                self._ui.gridLayout.addWidget(button)

    @pyqtSlot(str)
    def on_usage_changed(self, value):
        """切换当前脚本时UI变更"""
        self._ui.scriptsUsage.setText(value)

    @pyqtSlot(list)
    def on_button_uncheck(self, value):
        """关闭args时触发UI变更"""
        for btn in self._ui.groupBox.findChildren(QtWidgets.QPushButton):
            text = btn.text()
            if text in value:
                btn.setChecked(True)
            else:
                btn.setChecked(False)

    def on_print_text_clear(self):
        """清除控制台输出文本"""
        self._ui.outputTextBrowser.setText("")

    def output_written(self, text):
        """控制台文字输出"""
        cursor = self._ui.outputTextBrowser.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self._ui.outputTextBrowser.setTextCursor(cursor)
        self._ui.outputTextBrowser.ensureCursorVisible()

    def fave_list_context(self):
        """fave列表实现右键点击清除操作"""
        pop_menu = QMenu()
        action_delete = QAction(u'删除', self)
        action_delete.triggered.connect(self._controller.fave_remove)
        action_modify = QAction(u'修改', self)
        pop_menu.addAction(action_delete)
        pop_menu.addAction(action_modify)
        pop_menu.exec_(QtGui.QCursor.pos())

    def script_list_context(self):
        """script列表实现右键点击清除操作"""
        pop_menu = QMenu()
        action_delete = QAction(u'删除', self)
        action_delete.triggered.connect(self._controller.script_remove)
        action_modify = QAction(u'修改', self)
        pop_menu.addAction(action_delete)
        pop_menu.addAction(action_modify)
        pop_menu.exec_(QtGui.QCursor.pos())
