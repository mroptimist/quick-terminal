import sys
from PyQt5.QtWidgets import QApplication
from Model import Model
from Controller import Controller
from View import View


# sys.path.append("Project")


class App(QApplication):
    def __init__(self, sys_argv) -> None:
        super(App, self).__init__(sys_argv)
        self.model = Model()
        self.controller = Controller(self.model)
        self.view = View(self.model, self.controller)
        self.view.setWindowTitle("QuickTerminal")
        self.view.show()


if __name__ == '__main__':
    app = App(sys.argv)
    sys.exit(app.exec_())
