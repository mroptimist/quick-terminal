# QuickTerminal

#### 文档地址：
https://www.yuque.com/docs/share/032bbdd4-19d9-492d-a007-6a1f89f1b922?#

#### 介绍
MacOS下的GUI工具。基本上原理就是存储记录命令，可以点击直接运行。
支持运行Python脚本，适合添加一些轻量级的脚本，包括参数也进行了GUI化，不过需要自己在config.json里面配置，参数可以配置互斥。
在CommandLine页下可以选择要运行的命令行。
在添加界面可以选择添加python脚本或者命令行，python脚本直接拖入并点击添加，命令行可以键入或粘贴进来。

还不支持与终端有关的交互。

#### 软件架构
MVC..?

